import RPi.GPIO as GPIO
import time
import datetime
from gpiozero import LED

SENSORPIN = 25
GREENLED = 24
REDLED = 23

GPIO.setmode(GPIO.BCM)

def getSensorTime (pin):
    # Set the GPIO pin to an output, and set the output to low (0.0v)
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, GPIO.LOW)
    time.sleep(0.1)

    # Set the GPIO pin to an input, and measure the time taken to go from low to high
    GPIO.setup(pin, GPIO.IN)
    t1 = datetime.datetime.now()
    while (GPIO.input(pin) == GPIO.LOW):
        pass
    t2 = datetime.datetime.now()
    delta = t2 - t1
    milliseconds = delta.total_seconds() * 1000
    return milliseconds

def indicate (value):
    redLED.off()
    greenLED.off()

    if value < 100:
        greenLED.on()
    else:
        redLED.on()

try:
    redLED = LED(REDLED)
    greenLED = LED(GREENLED)
    redLED.off()
    greenLED.off()
    while True:
        sensorTime = getSensorTime (SENSORPIN)
        print (sensorTime)
        indicate (sensorTime)
        time.sleep(0.5)

finally:
    print ("cleaning up GPIO")
    GPIO.cleanup()
